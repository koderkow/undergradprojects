#read data
data <- read.csv(file.choose())

#insights
summary(data)
str(data)
colnames(data)

#remove unwanted columns
data$color <- NULL 
data$director_name <- NULL
data$director_facebook_likes <- NULL 
data$actor_1_facebook_likes <- NULL
data$actor_2_facebook_likes <- NULL 
data$actor_3_facebook_likes <- NULL
data$actor_1_name <- NULL 
data$actor_2_name <- NULL
data$actor_3_name <- NULL 
data$genres <- NULL
data$num_voted_users <- NULL 
data$num_user_for_reviews <- NULL
data$cast_total_facebook_likes <- NULL 
data$facenumber_in_poster <- NULL
data$plot_keywords <- NULL 
data$movie_imdb_link <- NULL
data$language <- NULL 
data$country <- NULL
data$aspect_ratio <- NULL
data$content_rating <- NULL

#calculate mean for missing duration datas
data$duration = ifelse(is.na(data$duration),
                       ave(data$duration, FUN= function(x) mean(x, na.rm=T)),
                       data$duration)

#convert duration from double back to numeric
data$duration <- as.numeric(round(data$duration,0))

#clear all rows with missing data
movies <- na.omit(data)
movies <- movies[-c(3221,3450,4398,4310,3878,3821,3274,
                    3771,4432)]

#Make profit column
movies$profit <- movies$gross - movies$budget

#Make did it profit column
movies$did_it_profit <- ifelse(movies$profit > 0, 'yes','no')

movies$did_it_profit = factor(movies$did_it_profit,
                              levels = c('no', 'yes'),
                              labels = c(0, 1))

summary(movies)

#Make training/test set
install.packages('caTools')
library(caTools)
set.seed(123)
split = sample.split(movies$profit, SplitRatio = 0.8)
training_set = subset(movies, split == TRUE)
test_set = subset(movies, split == FALSE)

#1 Backward Elimination Method--------------------------------------------------------------------------
mlr1 <- lm(formula = profit ~ num_critic_for_reviews + duration + gross + budget + imdb_score +
                  movie_facebook_likes + did_it_profit,
                data = movies)

summary(mlr1)
#2 Eliminate largest P value (did_it_profit)
mlr1 <- lm(formula = profit ~ num_critic_for_reviews + duration + gross + budget + imdb_score +
                  movie_facebook_likes,
                data = movies)

summary(mlr1)
#3 Eliminate largest P value (movie_facebook_likes)
mlr1 <- lm(formula = profit ~ num_critic_for_reviews + duration + gross + budget,
                data = training_set)

summary(mlr1)

#Backward Elim - Budget (imdb_score)
mlr1 <- lm(formula = profit ~ num_critic_for_reviews + duration + gross + imdb_score +
             movie_facebook_likes + did_it_profit,
           data = movies)
summary(mlr1)

#Backward Elim - Budget (duration)
mlr1 <- lm(formula = profit ~ num_critic_for_reviews + gross + movie_facebook_likes +
             did_it_profit,
           data = movies)
summary(mlr1)

y_pred <- predict(mlr1, newdata = test_set)
y_pred[1:10]
test_set[1:10, 'profit']

mean(test_set[, 'profit'])/1000000
mean(y_pred)/1000000

median(test_set[, 'profit'])/1000000
median(y_pred)/1000000

#END MULTI REGRESSION MODEL--------------------------------------------------------------------------

plot(mlr1)

library(ggplot2)

ggplot() +
  geom_point(aes(x = training_set$duration, 
                 y = training_set$profit),
             color = 'red') +
  geom_line(aes(x = training_set$duration,
                y = predict(regressor, newdata = training_set)),
            color = 'blue') +
  #coord_cartesian(xlim = c(0, 200), ylim = c(-4500000, 5000000)) +
  ggtitle('Salary vs Experience (Training Set)') +
  xlab('Years of Experience') +
  ylab('Salary')

#Profit vs Duration
ggplot() +
  geom_point(aes(x = test_set$duration, 
                 y = test_set$profit),
             color = 'red') +
  geom_line(aes(x = training_set$duration,
                y = predict(regressor, newdata = training_set)),
            color = 'blue') +
  coord_cartesian(xlim = c(60, 200), ylim = c(-200000000, 300000000)) +
  ggtitle('Profit vs Duration (Test Set)') +
  xlab('Duration') +
  ylab('Profit')
termplot(y_pred, terms='duration')
ggplot() +
  geom_point(aes(x = test_set$gross, 
                 y = test_set$profit),
             color = 'red') +
  geom_abline(aes(x = training_set$gross,
                  y = predict(regressor, newdata = training_set)),
              color = 'blue') +
  abline(min(y_pred),max(y_pred))
coord_cartesian(xlim = c(0, 6e+08), ylim = c(-200000000, 300000000)) +
  ggtitle('Profit vs Duration (Test Set)') +
  xlab('Duration') +
  ylab('Profit')


termplot(y_pred, terms='duration')

plot(y_pred)
abline(min(y_pred),max(y_pred))

#SIMPLE LINEAR MODELS & GRAPHS-----------------------------------------------------------
#gross
model1 <- lm(formula = profit ~ gross, data = test_set)
summary(model1)
plot(movies$profit ~ movies$gross, data = training_set, ylim=c(-1.0e+08,1.0e+08),
     xlab='Gross', ylab='Profit', main='Profit vs Gross', xlim=c(0,3e+08))
abline(model1, col='red')

#budget
model2 <- lm(formula = profit ~ budget, data = test_set)
summary(model2)
plot(movies$profit ~ movies$budget, data = training_set,
     xlab='Budget', ylab='Profit', main='Profit vs Budget',
     xlim=c(0,3.0e+08), ylim=c(-1.0e+08,4.0e+08))
abline(model2, col='red')

#duration
model3 <- lm(formula = profit ~ duration, data = test_set)
summary(model3)
plot(movies$profit ~ movies$duration, data = training_set,
     xlab='Duration', ylab='Profit', main='Profit vs Duration',
     xlim=c(0,300), ylim=c(-1.0e+08,4.0e+08))
abline(model3, col='red')

#critic
model4 <- lm(formula = profit ~ num_critic_for_reviews, data = test_set)
summary(model4)
plot(movies$profit ~ movies$num_critic_for_reviews, data = training_set,
     xlab='# of Critic Reviews', ylab='Profit', main='# of Critic Reviews',
     xlim=c(0,650), ylim=c(-1.0e+08,3.0e+08))
abline(model4, col='red')

#critic
model5 <- lm(formula = profit ~ movie_facebook_likes, data = test_set)
summary(model5)
plot(movies$profit ~ movies$movie_facebook_likes, data = training_set,
    xlab='# of Facebook Likes', ylab='Profit', main='Profit vs # of Facebook Likes',
    xlim=c(0,40000), ylim=c(-1.0e+08,2.0e+08))
abline(model5, col='red')

#imdb
model6 <- lm(formula = profit ~ imdb_score, data = test_set)
summary(model6)
plot(movies$profit ~ movies$imdb_score, data = training_set,
     xlab='IMDb Score', ylab='Profit', main='Profit vs IMDb Score',
     xlim=c(0,10), ylim=c(-1.0e+08,2.0e+08))
abline(model5, col='red')
#END SIMPLE LINEAR MODELS & GRAPHS---------------------------------------------------


  