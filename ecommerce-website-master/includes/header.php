<?php
if(session_status() == PHP_SESSION_NONE){
    session_start();
}
$count=0;

//retrieve cart content

if(isset($_SESSION['cart'])){
    $cart = $_SESSION['cart'];

    if($cart){
        $count = array_sum($cart);
    }
}

//set shopping cart image

$shoppingcart_img = (!$count) ? "shopcart.png":"shopcart.png";

//variables for a user's login, name, and role

$login = '';
$name = '';
$role = 0;

//if the user has logged in, retrieve login, name, and role.

if(isset($_SESSION['login']) AND isset($_SESSION['name']) AND isset($_SESSION['role'])) {
    $login = $_SESSION['login'];
    $name = $_SESSION['name'];
    $role = $_SESSION['role'];

}
?>



<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="style/css/boybandtheme.css" />
    <title><?php echo $page_title; ?></title>
</head>
<body>
<div id="wrapper">
    <div id="curdate">
        <?php
        date_default_timezone_set('America/New_York');
        echo date("l, F d, Y", time());
        ?>

    </div>
    <div id="navbar">
        <a href="index.php">Home</a>  ||
        <a href="listalbums.php">List Albums</a> ||
        <a href="searchalbums.php">Search Albums</a> ||

        <a href='aboutus.php'>About Us</a> ||
        <?php
        if($role == 1){
            echo "<a href='addalbum.php'>Add Album</a> || ";
        }
        if($role == 1 || $role == 2){
            echo "<a href='edituser.php'>Edit Details</a> || ";
        }
        if(empty($login))
            echo "<a href='loginform.php'>Login</a>";
        else{
            echo "<a href='logout.php'>Logout</a>";
            echo "<span id='navbarWELCOME' style ='color:white; margin-left:30px'>Welcome $name!</style>";
        }


        ?>
    </div>
    <table id="banner">
        <tr>
            <td>
                <img src="boyBandPics/BoyBand Extra.jpg" alt="Boy Band">
            </td>
            <td>
                <div id="maintitle"></div>
                <div id="subtitle"></div>
                <div class ='imageslider'>
                </div>
            </td>
            <td>
                <div id="shoppingcart">
                    <a href="showcart.php">
                        <img src='boyBandPics/<?=$shoppingcart_img ?>' style='width: 50px; border: none'/>
                        <br />
                        <span><?php echo $count ?> items</span>
                    </a>
                </div>
            </td>
        </tr>
    </table>
    <!-- main content body starts -->
    <div id="mainbody"></div>

