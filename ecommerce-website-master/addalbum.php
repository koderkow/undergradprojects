<?php

 //start session if it has not already started
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

//determine user's role
if (isset($_SESSION['role'])) {
    $role = $_SESSION['role'];
}

//deny access to the script if the user is not an administrator
if (!isset($role) OR $role != 1) {
    $error = "Access to this page is permitted for administrators only.";
    header("Location: error.php?m=$error");
    exit;
}


$page_title = "Add Album";
require_once 'includes/header.php';

?>

<h2>Add New Album</h2>
<form action="insertalbum.php" method="post">
    <table cellspacing="0" cellpadding="3" style="border: 1px solid silver; padding:5px; margin-bottom: 10px">
        <tr>
            <td style="text-align: right; width: 100px">Album Name: </td>
            <td><input name="album_name" type="text" size="100" required /></td>
        </tr>
        <tr>
            <td style="text-align: right">Artist:</td>
            <td>
                <select name="artist_id">
                    <option value="1">Backstreet Boys</option>
                    <option value="2">NSYNC</option>
                    <option value="3">One Direction</option>
                    <option value="4">Jonas Brothers</option>
                </select>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">Year Published: </td>
            <td><input name="year" type="text" required /></td>
        </tr>
        <tr>
            <td style="text-align: right">Price: </td>
            <td><input name="price" type="number" step="0.01" required /></td>
        </tr>
        <tr>
            <td style="text-align: right">Image: </td>
            <td><input name="image" type="text" size="100" required /></td>
        </tr>
        <tr>
            <td><h4>Bio Link:</h4></td>
            <td><textarea name="description" rows="6" cols="65"></textarea></td>
        </tr>
        
    </table>
    <div class="bookstore-button">
        <input type="submit" value="Add Album" />
        <input type="button" value="Cancel" onclick="window.location.href='listalbums.php'" />
    </div>
</form>

<?php
require_once 'includes/footer.php';