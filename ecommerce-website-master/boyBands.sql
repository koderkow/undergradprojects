-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 19, 2017 at 04:57 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boyBands`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `album_id` int(11) NOT NULL,
  `album_name` varchar(200) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `price` float NOT NULL,
  `image` varchar(100) NOT NULL,
  `bio` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`album_id`, `album_name`, `artist_id`, `year`, `price`, `image`, `bio`) VALUES
(1, 'Backstreet Boys', 1, 1996, 7.99, 'boyBandPics/backstreetboys.jpeg', 'backstreet.php'),
(2, 'Backstreet\'s Back', 1, 1997, 7.99, 'boyBandPics/backstreetsback.jpeg', 'backstreet.php'),
(3, 'Millennium', 1, 1999, 7.99, 'boyBandPics/millennium.jpeg', 'backstreet.php'),
(4, 'Black & Blue', 1, 2000, 7.99, 'boyBandPics/blackandblue.jpeg', 'backstreet.php'),
(5, 'Never Gone', 1, 2005, 8.99, 'boyBandPics/nevergone.jpeg', 'backstreet.php'),
(6, 'Unbreakable', 1, 2007, 9.99, 'boyBandPics/unbreakable.jpeg', 'backstreet.php'),
(7, 'This Is Us', 1, 2009, 9.99, 'boyBandPics/thisisus.jpeg', 'backstreet.php'),
(8, 'In a World Like This', 1, 2013, 10.99, 'boyBandPics/inaworldlikethis.jpeg', 'backstreet.php'),
(9, 'NSYNC', 2, 1997, 7.99, 'boyBandPics/nysync.jpeg', 'nsync.php'),
(10, 'No Strings Attached', 2, 2000, 7.99, 'boyBandPics/nostringsattached.jpeg', 'nsync.php'),
(11, 'Celebrity', 2, 2001, 8.99, 'boyBandPics/celebrity.jpeg', 'nsync.php'),
(12, 'Up All Night', 3, 2011, 10.99, 'boyBandPics/upallnight.jpeg', 'onedirection.php'),
(13, 'Take Me Home', 3, 2012, 10.99, 'boyBandPics/takemehome.jpeg', 'onedirection.php'),
(14, 'Midnight Memories', 3, 2013, 10.99, 'boyBandPics/midnightmemories.jpeg', 'onedirection.php'),
(15, 'Four', 3, 2014, 10.99, 'boyBandPics/four.jpeg', 'onedirection.php'),
(16, 'Made in the A.M.', 3, 2015, 10.99, 'boyBandPics/madeintheam.jpeg', 'onedirection.php'),
(17, 'It\'s About Time', 4, 2006, 9.99, 'boyBandPics/itsabouttime.jpeg', 'jonasbros.php'),
(18, 'Jonas Brothers', 4, 2007, 9.99, 'boyBandPics/jonasbrothers.jpg', 'jonasbros.php'),
(19, 'A Little Bit Longer', 4, 2008, 9.99, 'boyBandPics/alittlebitlonger.jpeg', 'jonasbros.php'),
(20, 'Lines, Vines, and Trying Times', 4, 2009, 9.99, 'boyBandPics/linesvines.jpeg', 'jonasbros.php');

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `artist_id` int(11) NOT NULL,
  `artist_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`artist_id`, `artist_name`) VALUES
(1, 'Backstreet Boys'),
(2, 'NSYNC'),
(3, 'One Direction'),
(4, 'Jonas Brothers');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `username`, `password`, `role`) VALUES
(1, 'Kyle', 'Harris', 'kow', 'password', 1),
(2, 'Saba', 'Ahmed', 'spa', 'password', 1),
(3, 'Prima', 'RiveraHernandez', 'justprima', 'password', 1),
(4, 'Lucas', 'Kooy', 'LaKooy', 'password', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`album_id`),
  ADD KEY `artist_id` (`artist_id`);

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`artist_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
  MODIFY `artist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `albums`
--
ALTER TABLE `albums`
  ADD CONSTRAINT `fk` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`artist_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
