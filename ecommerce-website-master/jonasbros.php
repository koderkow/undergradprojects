<?php
// This page displays the list of all albums in the database.

$page_title = "Jonas Brothers Bio";
require 'includes/header.php';
require_once('includes/database.php');

//SELECT statement
$sql = "SELECT album_id, album_name, year, price, artist_name, image "
    . "FROM artist, albums "
    . "WHERE artist.artist_id = albums.artist_id AND artist.artist_id = 4";

//execute the query
$query = @$conn->query($sql);

//Handle errors
if (!$query) {
    $errno = $conn->errno;
    $error = $conn->error;
    $conn->close();
    require 'includes/footer.php';
    die("Selection failed: ($errno) $error.");
}
?>

<h2>Jonas Brothers</h2>

   <center><img src="boyBandPics/biojonasbrothers.jpg" width="500px"></center>
    <br>
    <hr>

   <p class="bioText">The Jonas Brothers were an American pop rock band. Formed in 2005, they
       gained popularity from their appearances on the Disney Channel television network. 
       They consist of three brothers: Paul Kevin Jonas II, Joseph Adam Jonas, and Nicholas 
       Jerry Jonas. Raised in Wyckoff, New Jersey, the Jonas Brothers moved to Little 
       Falls, New Jersey in 2005, where they wrote their first record that made its Hollywood 
       release. In the summer of 2008, they starred in the Disney Channel Original Movie 
       Camp Rock and its sequel, Camp Rock 2: The Final Jam. They also starred as Kevin, Joe, 
       and Nick Lucas, the band JONAS, in their own Disney Channel series JONAS, which was 
       later re-branded for its second season as Jonas L.A. The show was eventually cancelled
       after two seasons. The band released four albums: It's About Time (2006), Jonas Brothers 
       (2007), A Little Bit Longer (2008), and Lines, Vines and Trying Times (2009).
    </p>
    <hr>
    <br>
   <center><img src="boyBandPics/bio2jonasbrothers.jpg" width="500px"></center>
    <br>
    <hr>

   <p class="bioText">In 2008, the group was nominated for the Best New Artist award at the 51st Grammy Awards
       and won the award for Breakthrough Artist at the American Music Awards. As of May 2009,
       before the release of Lines, Vines and Trying Times, they had sold over eight million
       albums worldwide. After a hiatus during 2010 and 2011 to pursue solo-projects, 
       the group reconciled in 2012 to record a new album, which was cancelled following their
       break-up on October 29, 2013. <a href="https://en.wikipedia.org/wiki/Jonas_Brothers">From Wikipedia</a>
    </p>
    <hr>
    <br>

   <h2>Jonas Brother's Albums</h2>

   <table id="albumlist" class="albumlist">

       <tr>
            <th class="col1"></th>
            <th class="col2"></th>
            <!--        <th class="col3">Year</th>
                    <th class="col4">Price</th>-->
        </tr>
        <!-- add PHP code here to list all books from the "books" table -->
        <?php
        while ($row = $query->fetch_assoc()) {

           echo "<tr>";
            // echo "<td>", $row["image"],"</td>";

           echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", "<img src='", $row['image'], "' alt='' style='width:150px'>", "</a></td>";

           echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", $row['album_name'], "</a> (" , $row['year'], ")","<br/>",
            "by " , $row['artist_name'], "<br/>",
            "$", $row['price'],"&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;","<br/>",
            "</td>";
            echo "</tr>";
        }
        ?>
    </table>

<?php
require 'includes/footer.php';

