<?php
// This page displays the list of all albums in the database.

$page_title = "Backstreet Boys Bio";
require 'includes/header.php';
require_once('includes/database.php');

//SELECT statement
$sql = "SELECT album_id, album_name, year, price, artist_name, image "
    . "FROM artist, albums "
    . "WHERE artist.artist_id = albums.artist_id AND artist.artist_id = 1";

//execute the query
$query = @$conn->query($sql);

//Handle errors
if (!$query) {
    $errno = $conn->errno;
    $error = $conn->error;
    $conn->close();
    require 'includes/footer.php';
    die("Selection failed: ($errno) $error.");
}
?>

    <h2>Backstreet Boys</h2>

    <center><img src="boyBandPics/bioBACKSTREET2.jpeg" width="500px"></center>
    <br>
    <hr>

   <p class="bioText">The Backstreet Boys (often abbreviated as BSB)[1] are an American vocal group,
       formed in Orlando, Florida in 1993. The group consists of AJ McLean, Howie D.,
       Nick Carter, Kevin Richardson, and Brian Littrell. The group rose to fame with
       their debut international album, Backstreet Boys (1996). In the following year
       they released their second international album Backstreet's Back (1997), and their
       U.S. debut album which continued the group's success worldwide. They rose to
       superstardom with their third studio album Millennium (1999) and its follow-up 
       album, Black & Blue (2000).
    </p>
    <hr>
    <br>

    <center><img src="boyBandPics/bioBACKSTREET.jpg" width="500px"></center>
    <br>
    <hr>

   <p class="bioText">After a two-year hiatus, they regrouped and released a comeback album Never Gone
       (2005). After the conclusion of the Never Gone Tour in 2006, Richardson left the
       group to pursue other interests. The group then released two albums as a quartet:
       Unbreakable (2007) and This Is Us (2009). In 2012, the group announced that Richardson
       had rejoined them permanently. In the following year they celebrated their 20th
       anniversary and released their first independent album, In a World Like This (2013).
       The group also released their first documentary movie, titled Backstreet Boys: Show
       'Em What You're Made Of in January 2015.
       <a href="https://en.wikipedia.org/wiki/Backstreet_Boys">From Wikipedia</a>
    </p>
    <hr>
    <br>

   <h2>Backstreet's Albums</h2>

   <table id="albumlist" class="albumlist">

       <tr>
            <th class="col1"></th>
            <th class="col2"></th>
            <!--        <th class="col3">Year</th>
                    <th class="col4">Price</th>-->
        </tr>
        <!-- add PHP code here to list all books from the "books" table -->
        <?php
        while ($row = $query->fetch_assoc()) {

           echo "<tr>";
            // echo "<td>", $row["image"],"</td>";

           echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", "<img src='", $row['image'], "' alt='' style='width:150px'>", "</a></td>";

           echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", $row['album_name'], "</a> (" , $row['year'], ")","<br/>",
            "by " , $row['artist_name'], "<br/>",
            "$", $row['price'],"&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;","<br/>",
            "</td>";
            echo "</tr>";
        }
        ?>
    </table>

<?php
require 'includes/footer.php';


