<?php
// This page displays the list of all albums in the database.

$page_title = "One Direction Bio";
require 'includes/header.php';
require_once('includes/database.php');

//SELECT statement
$sql = "SELECT album_id, album_name, year, price, artist_name, image "
    . "FROM artist, albums "
    . "WHERE artist.artist_id = albums.artist_id AND artist.artist_id = 3";

//execute the query
$query = @$conn->query($sql);

//Handle errors
if (!$query) {
    $errno = $conn->errno;
    $error = $conn->error;
    $conn->close();
    require 'includes/footer.php';
    die("Selection failed: ($errno) $error.");
}
?>

<h2>One Direction</h2>

   <center><img src="boyBandPics/bio2onedirection.png" width="500px"></center>
    <br>
    <hr>

   <p class = "bioText">One Direction (commonly abbreviated as 1D) are an English-Irish pop boy band based
       in London, composed of Niall Horan, Liam Payne, Harry Styles, Louis Tomlinson, and 
       previously, Zayn Malik until his departure from the band on 25 March 2015. The group
       signed with Simon Cowell's record label Syco Records after forming and finishing 
       third in the seventh series of the British televised singing competition The X Factor
       in 2010. Propelled to international success by social media, One Direction's five 
       albums, Up All Night (2011), Take Me Home (2012), Midnight Memories (2013), Four 
       (2014), and Made in the A.M. (2015), topped charts in most major markets, and generated
       hit singles including "What Makes You Beautiful", "Live While We're Young", "Best Song 
       Ever", "Story of My Life", and "Drag Me Down".
    </p>
    <hr>
    <br>

   <center><img src="boyBandPics/bioonedirection.jpg" width="500px"></center>
    <br>
    <hr>

   <p class = "bioText">Their awards include six Brit Awards, four MTV Video Music Awards, eleven MTV Europe
       Music Awards, seven American Music Awards (including Artist of the Year in 2014 and
       2015), and 27 Teen Choice Awards, among many others. According to Nick Gatfield, the
       chairman and chief executive of Sony Music Entertainment UK, One Direction represented
       a $50 million business empire by June 2012. They were proclaimed 2012's "Top New Artist"
       by Billboard. According to the Sunday Times Rich List, by April 2013, they had an
       estimated personal combined wealth of £25 million ($41.2m) making them the second-wealthiest
       musicians in the UK under 30 years of age. In 2014, Forbes listed them the second-highest
       earning celebrities under 30, with the group earning an estimated $75 million from June 2013
       to June 2014. In June 2015, Forbes listed their earnings at $130 million for the previous
       twelve months, and ranked them the fourth highest earning celebrities in the world.
       In 2016 Forbes ranked them the world's second highest earning celebrities. 
       <a href="https://en.wikipedia.org/wiki/One_Direction">From Wikipedia</a>
    </p>
    <hr>
    <br>

   <h2>One Direction's Albums</h2>

   <table id="albumlist" class="albumlist">

       <tr>
            <th class="col1"></th>
            <th class="col2"></th>
            <!--        <th class="col3">Year</th>
                    <th class="col4">Price</th>-->
        </tr>
        <!-- add PHP code here to list all books from the "books" table -->
        <?php
        while ($row = $query->fetch_assoc()) {

           echo "<tr>";
            // echo "<td>", $row["image"],"</td>";

           echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", "<img src='", $row['image'], "' alt='' style='width:150px'>", "</a></td>";

           echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", $row['album_name'], "</a> (" , $row['year'], ")","<br/>",
            "by " , $row['artist_name'], "<br/>",
            "$", $row['price'],"&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;","<br/>",
            "</td>";
            echo "</tr>";
        }
        ?>
    </table>

<?php
require 'includes/footer.php';
