<?php
/*
 * Author: Louie Zhu
 * Date: Jun 22, 2016
 * File: checkout.php
 * Description: this script simulates the checkout action. It emptys the shopping cart.
 *
 */

//start session if it has not already started
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

//check to see if the use has logged in
if (!isset($_SESSION['login'])) {
    header("Location: loginform.php");
    exit();
}

//update the cart
$_SESSION['cart'] = '';

$page_title = "Checkout";
require_once ('includes/header.php');
?>

    <h2>Checkout</h2>
    <p>You've done it. You completed your order for Boy Bands Extravaganza! Now it may take awhile for the CDs to be shipped to you, but that's only because they're CDs and no one does that stuff anymore.</p>

<?php
include ('includes/footer.php');
