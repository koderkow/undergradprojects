<?php
/** Author: Louie Zhu
 *  Date: 10/15/2015
 *  Description: This PHP script retrieves a user id from a url querystring.
 *  It then retrieves details of the specified user from the users table in the databate.
 *  At the end, it displays user details in a HTML table.
 */
$page_title = "Users details";

require_once ('includes/header.php');
require_once('includes/database.php');

//retrieve user id
if (!filter_has_var(INPUT_GET, 'id')) {
    echo "Error: user id was not found.";
    require_once ('includes/footer.php');
    exit();
}

$user_id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

//define the select statement
$sql = "SELECT * FROM users WHERE user_id=" . $user_id;

//execute the query
$query = $conn->query($sql);

//retrieve the results
$row = $query->fetch_assoc();


//Handle selection errors
if (!$query) {
    $errno = $conn->errno;
    $errmsg = $conn->error;
    echo "Selection failed with: ($errno) $errmsg<br/>\n";
    $conn->close();
    //include the footer
    require_once ('includes/footer.php');
    exit;
}
//display results in a table
?>

    <h2>User Details</h2>

    <table class="userdetails">
        <tr>
            <th>User ID</th>
            <td><?php echo $row['user_id'] ?></td>
        </tr>
        <tr>
            <th>Username</th>
            <td><?php echo $row['user_name'] ?> </td>
        </tr>
        <tr>
            <th>First Name</th>
            <td><?php echo $row['firstname'] ?> </td>
        </tr>
        <tr>
            <th>Last Name</th>
            <td><?php echo $row['lastname'] ?> </td>
        </tr>
    </table>
    <p>

    <form action="deleteuser.php" onsubmit="return confirm('Are you sure you want to delete the user?')">
        <input type="button" onclick="window.location.href = 'edituser.php?id=<?php echo $row['user_id'] ?>'" value="Edit">&nbsp;&nbsp;
        <input type="submit" value="Delete">&nbsp;&nbsp;
        <input type="button" onclick="window.location.href = 'listusers.php'" value="Cancel">
        <input type="hidden" name="id" value="<?php echo $row['user_id'] ?>">
    </form>


    </p>

<?php
// clean up resultsets when we're done with them!
$query->close();

// close the connection.
$conn->close();

//include the footer
require_once ('includes/footer.php');
