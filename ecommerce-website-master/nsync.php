<?php
// This page displays the list of all albums in the database.

$page_title = "NSYNC Bio";
require 'includes/header.php';
require_once('includes/database.php');

//SELECT statement
$sql = "SELECT album_id, album_name, year, price, artist_name, image "
    . "FROM artist, albums "
    . "WHERE artist.artist_id = albums.artist_id AND artist.artist_id = 2";

//execute the query
$query = @$conn->query($sql);

//Handle errors
if (!$query) {
    $errno = $conn->errno;
    $error = $conn->error;
    $conn->close();
    require 'includes/footer.php';
    die("Selection failed: ($errno) $error.");
}
?>

<h2>NSYNC</h2>

    <center><img src="boyBandPics/bioNSYNC.jpg" width="500px"></center>
    <br>
    <hr>

    <p class = "bioText">NSYNC (sometimes stylized as *NSYNC or 'N Sync) was an American boy band formed in Orlando,
        Florida in 1995 and launched in Germany by BMG Ariola Munich. NSYNC consisted of Justin Timberlake,
        JC Chasez, Chris Kirkpatrick, Joey Fatone, and Lance Bass. After heavily publicized legal battles with
        their former manager Lou Pearlman and former record label Bertelsmann Music Group,
        the group's second album, No Strings Attached, sold over one million copies in one day and
        2.42 million copies in one week, which was a record for over fifteen years. In addition to a host
        of Grammy Award nominations, NSYNC has performed at the World Series, the Super Bowl and the
        Olympic Games, and sang or recorded with Elton John, Stevie Wonder, Michael Jackson, Phil Collins,
        Celine Dion, Aerosmith, Britney Spears, Nelly, Left Eye, Mary J. Blige, country music supergroup Alabama,
        and Gloria Estefan.
    </p>
    <hr>
    <br>

    <center><img src="boyBandPics/bioNSYNC2.jpg" width="500px"></center>
    <br>
    <hr>

    <p class = "bioText">Although NSYNC announced the start of a "temporary hiatus" in early 2002, the band has not recorded new
        material since then. In 2007, Lance Bass confirmed that the group had "definitely broken up." They
        have sold over 70 million albums during their career, becoming the eighth-best-selling boy band in
        history. Rolling Stone recognized their instant success as one of the Top 25 Teen Idol Breakout Moments
        of all time. <a href="https://en.wikipedia.org/wiki/NSYNC">From Wikipedia</a>
    </p>
    <hr>
    <br>

    <h2>NSYNC's Albums</h2>

    <table id="albumlist" class="albumlist">

        <tr>
            <th class="col1"></th>
            <th class="col2"></th>
            <!--        <th class="col3">Year</th>
                    <th class="col4">Price</th>-->
        </tr>
        <!-- add PHP code here to list all books from the "books" table -->
        <?php
        while ($row = $query->fetch_assoc()) {

            echo "<tr>";
            // echo "<td>", $row["image"],"</td>";

            echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", "<img src='", $row['image'], "' alt='' style='width:150px'>", "</a></td>";

            echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", $row['album_name'], "</a> (" , $row['year'], ")","<br/>",
            "by " , $row['artist_name'], "<br/>",
            "$", $row['price'],"&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;","<br/>",
            "</td>";
            echo "</tr>";
        }
        ?>
    </table>

<?php
require 'includes/footer.php';