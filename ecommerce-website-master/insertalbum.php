<?php

//start session if it has not already started
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

//determine user's role
if (isset($_SESSION['role'])) {
    $role = $_SESSION['role'];
}

//deny access to the script if the user is not an administrator
if (!isset($role) OR $role != 1) {
    $error = "Access to this page is permitted for administrators only.";
    header("Location: error.php?m=$error");
    exit;
}

//Do not proceed if there are no post data
if (!$_POST) {
    $error = "Direct access to this scirpt is not allowed.";
    header("Location: error.php?m=$error");
    die();
}


//include code from database.php file
require_once('includes/database.php');

/* Retrieve book details. 
 * For security purpose, call the built-in function real_escape_string to 
 * escape special characters in a string for use in SQL statement.
 */
$album_name = $conn->real_escape_string(trim($_POST['album_name']));
$artist_id = $conn->real_escape_string(trim($_POST['artist_id']));
$year = $conn->real_escape_string(trim($_POST['year']));
$price = $conn->real_escape_string(trim($_POST['price']));
$image = $conn->real_escape_string(trim($_POST['image']));
$bio = $conn->real_escape_string(trim($_POST['bio']));

//SQL insert statement
$sql = "INSERT INTO albums VALUES (NULL, '$album_name', '$artist_id', '$year', '$price','$image', '$bio')";

//execute the query
$query = @$conn->query($sql);

//Handle errors
if (!$query) {
    $error = "Insertion failed: $conn->error.";
    $conn->close();
    header("Location: error.php?m=$error");
    die();
}

//determin the book id
$id = $conn->insert_id;

//close the database connection
$conn->close();
header("Location: albumdetails.php?id=$id&m=insert");
