<?php
// This page displays the list of all albums in the database.

$page_title = "About Us";
require 'includes/header.php';

?>
    <div class="team_Member">
        <div class="teamPicture"><img src="boyBandPics/aboutusPRIMA.JPG" height="200px" width="200px"/></div>
        <div class="teamAbout">
            <h3 class="memberName">Prima</h3>
            <p>I am Primavera RiveraHernandez and I don't really do much other than watch
                Netflix and do homework. Sometimes I read. My favorite boy band is
                Backstreet Boys. </p>
        </div>
    </div>

    <div class="team_Member">
        <div class="teamPicture"><img src="boyBandPics/aboutusSABA.JPG" height="200px" width="200px"/></div>
        <div class="teamAbout">
            <h3 class="memberName">Saba</h3>
            <p>I am Saba Ahmed, my major is Informatics with a specialization in pre-med.
                My favorite animal is a unicorn and my favorite boy band is
                The Jonas Brothers :)</p>

        </div>
    </div>

    <div class="team_Member">
        <div class="teamPicture"><img src="boyBandPics/aboutusLUCAS.jpg" height="200px" width="200px"/></div>
        <div class="teamAbout">
            <h3 class="memberName">Lucas</h3>
            <p>I'm Lucas I play video game. I also am a photographer. My favorite boy band is
                NSYNC.</p>

        </div>
    </div>

    <div class="team_Member">
        <div class="teamPicture"><img src="boyBandPics/aboutusKYLE.jpg" height="200px" width="200px"/></div>
        <div class="teamAbout">
            <h3 class="memberName">Kyle Harris</h3>
            <p>Hello, I am currently a first semester junior. I am in the
            informatics program with a custom specialization. My future plans are to head
            straight into the masters program for data analytics here at IUPUI.
            My favorite boy band is definitely NYSNC.</p>

        </div>
    </div>

<?php
require_once ('includes/footer.php');