<?php

$page_title = "Edit user details";

require_once ('includes/header.php');
require_once('includes/database.php');

//retrieve user id from a query string
if (!filter_has_var(INPUT_GET, 'id')) {
    echo "Error: user id was not found.";
    require_once ('includes/footer.php');
    exit();
}
$user_id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

//select statement 
$sql = "SELECT * FROM users WHERE user_id =" . $user_id;

// . means concactenate 
//execute the query
$query = $conn->query($sql);

//retrieve results 
$row = $query->fetch_assoc();

//Handle selection errors
if (!$query) {
    $errno = $conn->errno;
    $errmsg = $conn->error;
    echo "Selection failed with: ($errno) $errmsg<br/>\n";
    $conn->close();
    //include the footer
    require_once ('includes/footer.php');
    exit;
}
//display results in a table
?>

<h2>Edit User </h2>

<form name="edituser" action="updateuser.php" method="get">
    <table class="userdetails">
        <tr>
            <th>User ID</th>
            <td><input name="user_id" value="<?php echo $row['user_id'] ?>" readonly="readonly" /></td>
        </tr>
        <tr>
            <th>Username</th>
            <td><input name="username" value="<?php echo $row['username'] ?>" size="30" required /></td>
        </tr>
        <tr>
            <th>First Name</th>
            <td><input name="firstname" value="<?php echo $row['firstname'] ?>" size="30" required /></td>
        </tr>
        <tr>
            <th>Last Name</th>
            <td><input name="lastname" value="<?php echo $row['lastname'] ?>" size="30" required /></td>
        </tr>

    </table>
    <br>
    <input type="submit" value="Update">&nbsp;&nbsp;
    <input type="button" onclick="window.location.href='userdetails.php?id=<?php echo $row['user_id'] ?>'" value="Cancel">
</form>



<?php
// clean up resultsets when we're done with them!
$query->close();

// close the connection.
$conn->close();

//include the footer
require_once ('includes/footer.php');
?>

