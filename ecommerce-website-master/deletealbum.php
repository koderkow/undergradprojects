<?php

/*
 * Author: Primavera RiveraHernandez
 * Date: 16 April 2017
 * File: deletebook.php
 * Description: Delete Books
 *
 */
$page_title = "Album Store";
require_once 'includes/header.php';
require_once('includes/database.php');

//if there were problems retrieving book id, the script must end.
if(!filter_has_var(INPUT_GET, 'id')) {
    echo "Deletion cannot continue since there were problems retrieving book id";
    include ('includes/footer.php');
    exit;
}

//add your code here

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

$sql = "DELETE FROM albums WHERE album_id=$id";
 
//execute the query
 $query = @$conn->query($sql);
 
//Handle selection errors
if (!$query) {
    $errno = $conn->errno;
    $errmsg = $conn->error;
    echo "Selection failed with: ($errno) $errmsg<br/>\n";
    $conn->close();
    exit;
}



//close database connection
$conn->close();

//display a confirmation message
echo "You have successfully deleted the album from the database.<br><br>";

require_once 'includes/footer.php';