<?php
/*
 * Author: Louie Zhu
 * Date: Jun 10, 2016
 * File: showcart.php
 * Description: this script displays shopping cart content.
 *
 */

$page_title = "Shopping Cart";
require_once ('includes/header.php');
require_once('includes/database.php');
?>
    <h2>My Shopping Cart</h2>
<?php
if (!isset($_SESSION['cart']) || !$_SESSION['cart']) {
    echo "Your shopping cart is empty.<br><br>";
    include ('includes/footer.php');
    exit();
}

//proceed since the cart is not empty
$cart = $_SESSION['cart'];
?>
    <table class="albumlist">
        <tr>
            <th style="width: 500px">Title</th>
            <th style="width: 60px">Price</th>
            <th style="width: 60px">Quantity</th>
            <th style="width: 60px">Total</th>
        </tr>
        <?php
        //select statement
        $sql = "SELECT album_id, album_name, price FROM albums WHERE 0";

        foreach (array_keys($cart) as $id) {
            $sql .= " OR album_id=$id";
        }

        //execute the query
        $query = $conn->query($sql);

        //fetch albums and display them in a table
        while ($row = $query->fetch_assoc()) {
            $id = $row['album_id'];
            $album_name = $row['album_name'];
            $price = $row['price'];
            $qty = $cart[$id];
            $total =  $qty * $price;
            echo "<tr>",
            "<td><a href='albumdetails.php?id=$id'>$album_name</a></td>",
            "<td>$$price</td>",
            "<td>$qty</td>",
            "<td>$$total</td>",
            "</tr>";
        }
        ?>
    </table>
    <br>
    <div class="bookstore-button">
        <input type="button" value="Checkout" onclick="window.location.href = 'checkout.php'"/>
        <input type="button" value="Cancel" onclick="window.location.href = 'listalbums.php'" />
    </div>
    <br><br>

<?php
include ('includes/footer.php');
