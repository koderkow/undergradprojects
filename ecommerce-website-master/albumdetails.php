<?php

$page_title = "Album Details";
require_once ('includes/header.php');
require_once('includes/database.php');

//if book id cannot retrieved, terminate the script.
if (!filter_has_var(INPUT_GET, "id")) {
    $conn->close();
    require_once ('includes/footer.php');
    die("Your request cannot be processed since there was a problem retrieving book id.");
}

//retrieve book id from a query string variable.
$id = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);

//MySQL SELECT statement
$sql = "SELECT * "
        . "FROM artist, albums "
        . "WHERE artist.artist_id = albums.artist_id "
        . "AND album_id=$id";

//execute the query
$query = @$conn->query($sql);

//Handle errors
if (!$query) {
    $errno = $conn->errno;
    $error = $conn->error;
    $conn->close();
    require 'includes/footer.php';
    die("Selection failed: ($errno) $error.");
}

if (!$row = $query->fetch_assoc()) {
    $conn->close();
    require 'includes/footer.php';
    die("Book not found.");
}
?>
<h2>Album Details</h2>
<table id="details" class="albumdetails">
    <tr>
        <td class="col1">
           <img src="<?php echo $row['image'] ?>" alt="" style="width: 150px" />
        </td>
        <td class="col2">
            <h4>Album:</h4>
            <h4>Artist:</h4>
            <h4>Year:</h4>
            <h4>Price:</h4>
        </td>
        <td class="col3">
            <p><?php echo $row['album_name'] ?></p>
            <p><a href=' <?=$row['bio']?>'><?= $row['artist_name']?></a></p>
            <p><?php echo $row['year'] ?></p>
            <p>$<?php echo $row['price'] ?></p>
        </td>
        <td class="col4">
            <audio id="myAudio"></audio>
            <img src='boyBandPics/listenIcon.png'  alt="Play Button" style='width:200px'  onclick="StartOrStop('music/<?php echo $row['album_name'] ?>.mp3')">


            <center>
            <a href="addtocart.php?id=<?= $id ?>">
                <img src="boyBandPics/addtocart_button.png" />
            </a>
            </center>
        </td>
    </tr>
</table>
<script src="js/main.js"></script>
<?php
$confirm = "";
if (isset($_GET['m'])) {
    if ($_GET['m'] == "insert") {
        $confirm = "You have successfully added the new album.";
    } else if ($_GET['m'] == "update") {
        $confirm = "Your album has been successfully updated.";
    }
}

//display the following buttons only if the user's role is 1.
if (isset($role) AND $role == 1) {
    ?>
    <div class="bookstore-button">
        <input type="button" 
               onclick="window.location.href = 'editalbum.php?id=<?= $id ?>'"   
               value="Edit">
        <input type="button" 
               onclick="window.location.href = 'deletealbum.php?id=<?= $id ?>'"   
               value="Delete">
        <input type="button" 
               onclick="window.location.href = 'listalbums.php'" 
               value="Cancel">
        <div style="color: red; display: inline-block;"><?= $confirm ?></div>
    </div>
    <?php
}
require_once ('includes/footer.php');