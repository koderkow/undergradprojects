<?php

//start session if it has not already started
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

//if session variable cart already exists, retrieve it; otherwise create it and initialize it with an array
if (isset($_SESSION['cart'])) {
    $cart = $_SESSION['cart'];
} else {
    $cart = array();  //initialize an empty array
}

//if album id cannot be found, or it is not an integer, terminate script.
if (!isset($_GET['id']) || !(int)$_GET['id']) {
    $error = "Invalid album id detected. Operation cannot proceed.";
    header("Location: error.php?m=$error");
    die();
}

//retrieve album id and make sure it is a numeric value.
$id = $_GET['id'];

/*
 * If the same album alreadt exists in the shopping card, increment the quantity by 1.
 * If not, add the album id into the cart and set the quantity to 1.
 */
if (array_key_exists($id, $cart)) {
    $cart[$id] = $cart[$id] + 1;
} else {
    $cart[$id] = 1;
}

//update the cart
$_SESSION['cart'] = $cart;

//redirect user to showcart.php
header('Location: showcart.php');