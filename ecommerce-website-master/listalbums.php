<?php
// This page displays the list of all albums in the database.

$page_title = "Albums in Our Store";
require 'includes/header.php';
require_once('includes/database.php');

//SELECT statement
$sql = "SELECT album_id, album_name, year, price, artist_name, image, bio "
        . "FROM artist, albums "
        . "WHERE artist.artist_id = albums.artist_id ";

//execute the query
$query = @$conn->query($sql);

//Handle errors
if (!$query) {
    $errno = $conn->errno;
    $error = $conn->error;
    $conn->close();
    require 'includes/footer.php';
    die("Selection failed: ($errno) $error.");
}
?>


<h2>Albums in Our Store</h2>

<table id="albumlist" class="albumlist">
    
    <tr>
        <th class="col1"></th>
        <th class="col2"></th>
<!--        <th class="col3">Year</th>
        <th class="col4">Price</th>-->
    </tr>
    <!-- add PHP code here to list all books from the "books" table -->
    <?php


    while ($row = $query->fetch_assoc()) {
        
        echo "<tr>";
        //image
        echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", "<img src='", $row['image'], "' alt='' style='width:150px'>", "</a></td>";
        //album name
        echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", $row['album_name'], "</a> (" , $row['year'], ")","<br/>",
                //hyperlink band name to bio
                "by ", "<a href='", $row['bio'],"'>",$row['artist_name'],"</a>","<br/>",
                //price with audio button
                "$", $row['price'],"&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;","<br/>",
                "</td>";

        echo "</tr>";
    }
    ?>
</table>

<?php
require 'includes/footer.php';
//  this is the hyperlink code for album details.      echo "<td><a href='bookdetails.php?id=", $row['id'], "'>", $row['title'], "</a></td>";

