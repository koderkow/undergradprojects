<?php

//start session if it has not already started
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

//determine user's role
if (isset($_SESSION['role'])) {
    $role = $_SESSION['role'];
}

//deny access to the script if the user is not an administrator
if (!isset($role) OR $role != 1) {
    $error = "Access to this page is permitted for administrators only.";
    header("Location: error.php?m=$error");
    exit;
}


$page_title = "Edit Album Details";
require_once ('includes/header.php');
require_once('includes/database.php');

//if book id cannot be retrieved, terminate the script.
if (!isset($_GET['id'])) {
    $error = "Your request cannot be processed since there was a problem retrieving book id.";
    $conn->close();
    header("Location: error.php?m=$error");
    die();
}

//retrieve book id from a query string variable.
$id = $_GET['id'];

//MySQL SELECT statement
$sql = "SELECT * FROM artist,albums WHERE artist.artist_id = albums.artist_id AND album_id=$id";

//execute the query
$query = @$conn->query($sql);

//Handle errors
if (!$query) {
    $error = "Selection failed: " . $conn->error;
    $conn->close();
    header("Location: error.php?m=$error");
    die();
}

$row = $query->fetch_assoc();
if (!$row) {
    $error = "Book not found";
    $conn->close();
    header("Location: error.php?m=$error");
    die();
}
?>

<h2>Edit Album Details</h2>
<form action="updatealbum.php" method="post">
    <table class="bookdetails">
        <tr>
            <td style="width: 40px"><h4>Album Name:</h4></td>
            <td style="width: 200px"><input name="album_name" size="80" value="<?php echo $row['album_name'] ?>" required></td>
        </tr>
        <tr>
            <td><h4>Artist:</h4></td>
            <td><select name="artist_id">
                    <option value="1">Backstreet Boys</option>
                    <option value="2">NSYNC</option>
                    <option value="3">One Direction</option>
                    <option value="4">Jonas Brothers</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><h4>Year Published:</h4></td>
            <td><input name="year" value="<?php echo $row['year'] ?>" required></td>
        </tr>
        <tr>
            <td><h4>Price:</h4></td>
            <td><input name="price" type="number" step="0.01" value="<?php echo $row['price'] ?>" required></td>
        </tr>
        <tr>
            <td><h4>Image</h4></td>
            <td><input name="image" size="120" value="<?php echo $row['image'] ?>" required></td>
        </tr>
        <tr>
            <td><h4>Bio Link:</h4></td>
            <td><textarea name="bio" rows="6" cols="65"><?php echo $row['bio'] ?></textarea></td>
        </tr>
    </table>
    <div class="bookstore-button">
        <input type="hidden" name="id" value="<?php echo $id ?>" />
        <input type="submit" value="Update" />
        <input type="button" value="Cancel" onclick="window.location.href = 'albumdetails.php?id=<?= $id ?>'" />
    </div>
</form>
<?php
// close the connection.
$conn->close();
require_once 'includes/footer.php';