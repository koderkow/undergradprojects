<?php
$page_title = "Search Albums results";

require_once ('includes/header.php');
require_once('includes/database.php');

if (filter_has_var(INPUT_GET, "terms")) {
    $terms_str = filter_input(INPUT_GET, 'terms', FILTER_SANITIZE_STRING);
} else {
    echo "There was not search terms found.";
    include ('includes/footer.php');
    exit;
}

//explode the search terms into an array
$terms = explode(" ", $terms_str);

//select statement using pattern search. Multiple terms are concatnated in the loop.

//SEARCH FOR ARTIST NAME
$sql = "SELECT album_id, album_name, year, price, artist_name, image, bio "
        . "FROM artist, albums "
        . "WHERE artist.artist_id = albums.artist_id "
        . "AND 1";
foreach ($terms as $term) {
    $sql .= " AND artist_name LIKE '%$term%'";
}

//SEARCH FOR ALBUM NAME
$sql2 = "SELECT album_id, album_name, year, price, artist_name, image, bio "
    . "FROM artist, albums "
    . "WHERE artist.artist_id = albums.artist_id "
    . "AND 1";
foreach ($terms as $term) {
    $sql2 .= " AND album_name LIKE '%$term%'";
}
//execute the query
$query = $conn->query($sql);

//Handle selection errors
if (!$query) {
    $errno = $conn->errno;
    $errmsg = $conn->error;
    echo "Selection failed with: ($errno) $errmsg.";
    $conn->close();
    include ('includes/footer.php');
    exit;
}

echo "<h2>Albums: $terms_str</h2>";



//display results in a table
//if ($query->num_rows == 0) {
//    echo "Your search <i>'$terms_str'</i> did not match any albums in our inventory";
//    include ('includes/footer.php');
//    exit;
//}
?>
    <form action="searchalbumsresults.php" method="get">
        <input type="text" name="terms" size="40" required />&nbsp;&nbsp;
        <input type="submit" name="Submit" id="Submit" value="Search Album" />
    </form>
<table id="albumlist" class="albumlist">
    <tr>
        <th class="col1"></th>
        <th class="col2"></th>
        <!--        <th class="col3">Year</th>
                <th class="col4">Price</th>-->
    </tr>

    <?php
    //PRINT OUT ARTIST NAME MATCHES
    while ($row = $query->fetch_assoc()) {

        echo "<tr>";
        //image
        echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", "<img src='", $row['image'], "' alt='' style='width:150px'>", "</a></td>";
        //album name
        echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", $row['album_name'], "</a> (" , $row['year'], ")","<br/>",
            //hyperlink band name to bio
        "by ", "<a href='", $row['bio'],"'>",$row['artist_name'],"</a>","<br/>",
            //price with audio button
        "$", $row['price'],"&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;","<br/>",
        "</td>";

        echo "</tr>";
    }

    $query = $conn->query($sql2);

    //Handle selection errors
    if (!$query) {
        $errno = $conn->errno;
        $errmsg = $conn->error;
        echo "Selection failed with: ($errno) $errmsg.";
        $conn->close();
        include ('includes/footer.php');
        exit;
    }

    //CHECK TO SEE IF THERE ARE ANY RESULTS
    if ($query->num_rows == 0) {
        echo "Your search <i>'$terms_str'</i> did not match any albums in our inventory";
        include ('includes/footer.php');
        exit;
    }

    //ALBUM NAME
    while ($row = $query->fetch_assoc()) {
        //THIS ELIMINATES DOUBLE MATCHES IE: ARTIST:NSYNC ALBUM:NSYNC
        if ($row['album_name'] != $row['artist_name']){
            echo "<tr>";
            //image
            echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", "<img src='", $row['image'], "' alt='' style='width:150px'>", "</a></td>";
            //album name
            echo "<td><a href='albumdetails.php?id=", $row['album_id'], "'>", $row['album_name'], "</a> (", $row['year'], ")", "<br/>",
                //hyperlink band name to bio
            "by ", "<a href='", $row['bio'], "'>", $row['artist_name'], "</a>", "<br/>",
                //price with audio button
            "$", $row['price'], "&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;", "<br/>",
            "</td>";

            echo "</tr>";
        }
    }
    ?>
</table>

<?php
// clean up resultsets when we're done with them!
$query->close();

// close the connection.
$conn->close();

include ('includes/footer.php');

